// -*- mode: c++; c-basic-offset: 2; indent-tabs-mode: nil; -*-
// Small example how to scroll text.
//
// This code is public domain
// (but note, that the led-matrix library this depends on is GPL v2)

#include "led-matrix.h"
#include "graphics.h"

#include <string>

#include <getopt.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "easywsclient/easywsclient.hpp"
#include "easywsclient/easywsclient.cpp" // <-- include only if you don't want compile separately
#include <memory>
#include <thread>
#include <fstream>
#include <iostream>

#define LINE0_Y 7
#define LINE1_Y 96

#define SERVER "ws://192.168.100.3:8770"
#define FONT2_PATH "../../otf2bdf/bdffonts/uni30.bdf"

using namespace rgb_matrix;
using easywsclient::WebSocket;

volatile bool interrupt_received = false;
static void InterruptHandler(int signo) {
  interrupt_received = true;
}

static int usage(const char *progname) {
  fprintf(stderr, "usage: %s [options] <text>\n", progname);
  fprintf(stderr, "Takes text and scrolls it with speed -s\n");
  fprintf(stderr, "Options:\n");
  rgb_matrix::PrintMatrixFlags(stderr);
  fprintf(stderr,
          "\t-s <speed>        : Approximate letters per second.\n"
          "\t-l <loop-count>   : Number of loops through the text. "
          "-1 for endless (default)\n"
          "\t-f <font-file>    : Use given font.\n"
          "\t-b <brightness>   : Sets brightness percent. Default: 100.\n"
          "\t-x <x-origin>     : X-Origin of displaying text (Default: 0)\n"
          "\t-y <y-origin>     : Y-Origin of displaying text (Default: 0)\n"
          "\t-S <spacing>      : Spacing pixels between letters (Default: 0)\n"
          "\n"
          "\t-C <r,g,b>        : Color. Default 255,255,0\n"
          "\t-B <r,g,b>        : Background-Color. Default 0,0,0\n"
          "\t-O <r,g,b>        : Outline-Color, e.g. to increase contrast.\n"
          );
  return 1;
}

static bool parseColor(Color *c, const char *str) {
  return sscanf(str, "%hhu,%hhu,%hhu", &c->r, &c->g, &c->b) == 3;
}

static bool FullSaturation(const Color &c) {
  return (c.r == 0 || c.r == 255)
    && (c.g == 0 || c.g == 255)
    && (c.b == 0 || c.b == 255);
}


struct matrix_message
{

  matrix_message(std::string t_message)
    :line(std::stoi(t_message.substr(0,1)))
    ,scroll(std::stoi(t_message.substr(1,1)))
    ,message(t_message.substr(2, t_message.length() - 2))
  {
  }

  int line;
  int scroll;
  std::string message;


};


int main(int argc, char *argv[]) {


  std::string server_url;
  std::ifstream server_file("./server.txt");

  matrix_message line0("0000");
  matrix_message line1("1111");


  RGBMatrix::Options matrix_options;
  rgb_matrix::RuntimeOptions runtime_opt;
  if (!rgb_matrix::ParseOptionsFromFlags(&argc, &argv,
                                         &matrix_options, &runtime_opt)) {
    return usage(argv[0]);
  }

  Color color(255, 255, 0);
  Color bg_color(0, 0, 0);
  Color outline_color(0,0,0);
  bool with_outline = false;

  const char *bdf_font_file = NULL;
  
  /* x_origin is set just right of the screen */
  int x_scroll_orig = (matrix_options.chain_length * matrix_options.cols) + 5;
  int x_orig = 0;
  int y_orig = 0;
  int brightness = 100;
  int letter_spacing = 0;
  float speed = 7.0f;
  int loops = -1;

  int opt;
  while ((opt = getopt(argc, argv, "x:y:f:C:B:O:b:S:s:l:")) != -1) {
    switch (opt) {
    case 's': speed = atof(optarg); break;
    case 'l': loops = atoi(optarg); break;
    case 'b': brightness = atoi(optarg); break;
    case 'x': x_orig = atoi(optarg); break;
    case 'y': y_orig = atoi(optarg); break;
    case 'f': bdf_font_file = strdup(optarg); break;
    case 'S': letter_spacing = atoi(optarg); break;
    case 'C':
      if (!parseColor(&color, optarg)) {
        fprintf(stderr, "Invalid color spec: %s\n", optarg);
        return usage(argv[0]);
      }
      break;
    case 'B':
      if (!parseColor(&bg_color, optarg)) {
        fprintf(stderr, "Invalid background color spec: %s\n", optarg);
        return usage(argv[0]);
      }
      break;
    case 'O':
      if (!parseColor(&outline_color, optarg)) {
        fprintf(stderr, "Invalid outline color spec: %s\n", optarg);
        return usage(argv[0]);
      }
      with_outline = true;
      break;
    default:
      return usage(argv[0]);
    }
  }

  if (bdf_font_file == NULL) {
    fprintf(stderr, "Need to specify BDF font-file with -f\n");
    return usage(argv[0]);
  }

  /*
   * Load font. This needs to be a filename with a bdf bitmap font.
   */
  rgb_matrix::Font font;
  if (!font.LoadFont(bdf_font_file)) {
    fprintf(stderr, "Couldn't load font '%s'\n", bdf_font_file);
    return 1;
  }

  rgb_matrix::Font font2;

  if (!font2.LoadFont(FONT2_PATH)) {
    fprintf(stderr, "Couldn't load font2 '%s'\n", FONT2_PATH);
    return 1;
  }

  /*
   * If we want an outline around the font, we create a new font with
   * the original font as a template that is just an outline font.
   */
  rgb_matrix::Font *outline_font = NULL;
  if (with_outline) {
    outline_font = font.CreateOutlineFont();
  }

  if (brightness < 1 || brightness > 100) {
    fprintf(stderr, "Brightness is outside usable range.\n");
    return 1;
  }

  RGBMatrix *canvas = rgb_matrix::CreateMatrixFromOptions(matrix_options,
                                                          runtime_opt);
  if (canvas == NULL)
    return 1;

  canvas->SetBrightness(brightness);

  const bool all_extreme_colors = (brightness == 100)
    && FullSaturation(color)
    && FullSaturation(bg_color)
    && FullSaturation(outline_color);
  if (all_extreme_colors)
    canvas->SetPWMBits(1);

  int x0 = x_orig;
  int y0 = y_orig;
  int x1 = x_orig;
  int y1 = y_orig;

  int length0 = 0;
  int length1 = 0;

  signal(SIGTERM, InterruptHandler);
  signal(SIGINT, InterruptHandler);

  printf("CTRL-C for exit.\n");

  // Create a new canvas to be used with led_matrix_swap_on_vsync
  FrameCanvas *offscreen_canvas = canvas->CreateFrameCanvas();

  int delay_speed_usec = 1000000 / speed / font.CharacterWidth('W');
  if (delay_speed_usec < 0) delay_speed_usec = 2000;


  // Websocket client
  std::getline(server_file, server_url);

  printf("Connecting to server %s\n", server_url.c_str());

  std::thread socket_thread([&]() {

    // std::unique_ptr<WebSocket> ws(WebSocket::from_url(server_url));
    std::shared_ptr<WebSocket> ws(WebSocket::from_url(server_url));
    while (1) {

      if(ws == NULL || ws->getReadyState() == WebSocket::CLOSED) {

        printf("Connecting to server %s\n", server_url.c_str());
        ws.reset(WebSocket::from_url(server_url));
        usleep(1000000);
        continue;
      }

      // WebSocket::pointer wsp = &*ws; // <-- because a unique_ptr cannot be copied into a lambda
      ws->poll();
      ws->dispatch([&](const std::string & message) {

          printf("received [%s]\n", message.c_str());

          matrix_message new_message(message);

          printf("line %d, scroll %d, message %s\n", new_message.line, new_message.scroll, new_message.message.c_str());
          if (new_message.line == 0) {
            line0 = new_message;

            rgb_matrix::DrawText(canvas, font, 0, y_orig + LINE0_Y,
                    color, outline_font ? NULL : &bg_color, line0.message.c_str(),
                    letter_spacing);
          }
          else {  
            line1 = new_message;

            rgb_matrix::DrawText(canvas, font2, 0, LINE1_Y,
                    color, outline_font ? NULL : &bg_color, line1.message.c_str(),
                    letter_spacing);


          }                       

      });
    }

  });
  



  while (!interrupt_received) {

      if (!line0.scroll && !line1.scroll) {
        
        sleep(2);

      } else {

        offscreen_canvas->Clear(); // clear canvas
        
        if (line0.scroll) {
          
          // length = holds how many pixels our text takes up
          length0 = rgb_matrix::DrawText(offscreen_canvas, font,
                                        x0, y_orig + LINE0_Y,
                                        color, outline_font ? NULL : &bg_color,
                                        line0.message.c_str(), letter_spacing);

          if (--x0 + length0 < 0) {
            x0 = x_scroll_orig;
          }

        } else {

          rgb_matrix::DrawText(canvas, font, 0, y_orig + LINE0_Y,
                    color, outline_font ? NULL : &bg_color, line0.message.c_str(),
                    letter_spacing);

        }

        if (line1.scroll) {

          // length = holds how many pixels our text takes up
          length1 = rgb_matrix::DrawText(offscreen_canvas, font2,
                                        x1, LINE1_Y,
                                        color, outline_font ? NULL : &bg_color,
                                        line1.message.c_str(), letter_spacing);

          if (--x1 + length1 < 0) {
            x1 = x_scroll_orig;
          }

        } else {

          rgb_matrix::DrawText(canvas, font2, 0, LINE1_Y,
                    color, outline_font ? NULL : &bg_color, line1.message.c_str(),
                    letter_spacing);

        }
        
        usleep(delay_speed_usec);
        // Swap the offscreen_canvas with canvas on vsync, avoids flickering
        offscreen_canvas = canvas->SwapOnVSync(offscreen_canvas);

      }
    
  }

  // Finished. Shut down the RGB matrix.
  canvas->Clear();
  delete canvas;

  // socket_thread.join();

  return 0;
}
