// Compile with:
// g++ -std=gnu++0x example-client-cpp11.cpp -o example-client-cpp11
#include "easywsclient.hpp"
#include "easywsclient.cpp" // <-- include only if you don't want compile separately

#include <assert.h>
#include <stdio.h>
#include <string>
#include <memory>
#include <thread>
#include <chrono>

int main()
{ 

    using easywsclient::WebSocket;

    std::unique_ptr<WebSocket> ws(WebSocket::from_url(SERVER));
    
    while (1) {

        if (!ws || ws->getReadyState() == WebSocket::CLOSED) {
            printf("Connecting to %s...\n", SERVER);
            ws.reset(WebSocket::from_url(SERVER));
            std::this_thread::sleep_for (std::chrono::seconds(3));
            continue;
        }

        WebSocket::pointer wsp = &*ws; // <-- because a unique_ptr cannot be copied into a lambda
        ws->poll();
        ws->dispatch([wsp](const std::string & message) {
            printf(">>> %s\n", message.c_str());
            if (message == "world") { wsp->close(); }
        });
    }

    return 0;
}
