

import sys
import time
import os
import keyboard
import json

from simple_websocket_server import WebSocketServer, WebSocket
try:
    import thread
except ImportError:
    import _thread as thread



def listToString(s):  
    
    # initialize an empty string 
    str1 = ""  
    
    # traverse in the string   
    for ele in s:  
        str1 += ele   
    
    # return string   
    return str1  

##########################################################
wsclients = []

def ws_send(mess):
    if len(wsclients) > 0:
        print ("send: ", mess)
        wsclients[0].send_message(mess)

class WsClient(WebSocket):

    def handle(self):
    # self.send_message(self.data)
        print "received {}".format(self.data)

    def connected(self):
        print(self.address, 'connected')
        if len(wsclients) == 0:
            wsclients.append(self)
        else:
            wsclients[0] = self

	def handle_close(self):
		print(self.address, 'closed')

def server_thread():
	print "WebSocket server started, port {}".format(8765)
	server = WebSocketServer('', 8765, WsClient)
	server.serve_forever()

########################################################



class KeyBoard:
    def start(self):
        
        thread.start_new_thread(self.thread, ())

    def thread(self):

        keyboard.on_press(self.on_key_press)

        while True:
            recorded = keyboard.record(until='enter')
            
            pressed = ""
            for event in recorded:
                
                ev = json.loads(event.to_json())
                # print ev
                if ev['event_type'] == "down":
                    val = ev['name']
                    if val == 'space':
                        pressed += ' '
                    elif val == 'enter':
                        pressed += ''
                    else:
                        pressed += val
            print ("pressed: ", pressed)
            ws_send(pressed)

    def on_key_press(self, event):
        ev = json.loads(event.to_json())

        val = ev['name']
        if   val == "up":
            print "UP"
        elif val == "down":
            print "DOWN"
        elif val == "right":
            print "RIGHT"
        elif val == "left":
            print "LEFT"

 

if __name__ == "__main__":

    thread.start_new_thread(server_thread, ())

    keyinput = KeyBoard()
    keyinput.start()

    while True:
        time.sleep(10)