from simple_websocket_server import WebSocketServer, WebSocket
try:
    import thread
except ImportError:
    import _thread as thread
import time

clients = []


class WsServer:
	def __init__(self):
		self.clients = []

	def start(self):
		self.server = WebSocketServer('', 8765, WsClient)
		self.server.serve_forever()


	
class WsClient(WebSocket):

	def handle(self):
		# self.send_message(self.data)
		print "received {}".format(self.data)

	def connected(self):
		print(self.address, 'connected')
		clients.append(self)

	def handle_close(self):
		print(self.address, 'closed')

def server_thread():
	print "WebSocket server started, port {}".format(8765)
	server = WebSocketServer('', 8765, WsClient)
	server.serve_forever()


if __name__ == "__main__":

	# 
	thread.start_new_thread(server_thread, ())

	# server = WsServer()
	
	# server.start()

	while True:
		time.sleep(1)
		
		# server.send_message('hello')