

import sys
import time
import os
from builtins import input
from pynput import keyboard
from pynput.keyboard import Key
from simple_websocket_server import WebSocketServer, WebSocket
try:
    import thread
except ImportError:
    import _thread as thread



def listToString(s):  
    
    # initialize an empty string 
    str1 = ""  
    
    # traverse in the string   
    for ele in s:  
        str1 += ele   
    
    # return string   
    return str1  

##########################################################
wsclients = []

def ws_send(mess):
    if len(wsclients) > 0:
        wsclients[0].send_message(mess)

class WsClient(WebSocket):

	def handle(self):
		# self.send_message(self.data)
		print "received {}".format(self.data)

	def connected(self):
		print(self.address, 'connected')
		wsclients.append(self)

	def handle_close(self):
		print(self.address, 'closed')

def server_thread():
	print "WebSocket server started, port {}".format(8765)
	server = WebSocketServer('', 8765, WsClient)
	server.serve_forever()

########################################################



class KeyInput:
    def __init__(self):
        self.input = list()

        self.listener = keyboard.Listener(
                on_press = self.key_on_press,
                on_release = self.key_on_release)

        self.listener.start()


    def key_on_press(self, key):
        
        try:
            if type(key) == type(Key.enter):
                if key == Key.enter:
                    print "Entter ", self.input
                    ws_send(listToString(self.input))
                    self.input = list()
                elif key == Key.backspace:
                    print "remove ", self.input[len(self.input)-1]
                    self.input.pop()
                elif key == Key.space:
                    self.input.append(' ')
            else:
                print "pressed ", key.char
                self.input.append(key.char)
        except:
            time.sleep(0.1)


    def key_on_release(self, key):
        if key == keyboard.Key.esc:
            # Stop listener
            return False

    
 
if __name__ == "__main__":

    thread.start_new_thread(server_thread, ())

    keyinput = KeyInput()
    while True:
        time.sleep(10)