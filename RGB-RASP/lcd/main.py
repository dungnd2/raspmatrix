# requires RPi_I2C_driver.py
import RPi_I2C_driver

import sys
# sys.path.append('./keyboard')
# import keyboard

import signal
import time
import os
import keyboard
import json
try:
    import thread
except ImportError:
    import _thread as thread

import threading 

from simple_websocket_server import WebSocketServer, WebSocket

WSPORT = 8765

COLS_NO = 20
ROWS_NO = 2
POINTER_CHAR = '_'


##########################################################
wsclients = []

def ws_send(mess):
    if len(wsclients) > 0:
        print ("send: ", mess)
        wsclients[0].send_message(mess)


class WsServer:
    def __init__(self):
        self.matrix_cli = ""
        self.keyboard_cli = ""
    def on_open(self, cli):

        if cli.address[0] == "127.0.0.1":
            print "keyboard connected ", cli.address
            self.keyboard_cli = cli 
        else:
            print "matrix connected ", cli.address
            self.matrix_cli = cli
    
    def matrix_send(self, message):

        print ("send matrix: ", message)

        if self.matrix_cli != "":
            self.matrix_cli.send_message(message)


class WsClient(WebSocket):


    def handle(self):
    # self.send_message(self.data)
        print "received {}".format(self.data)
        lcd.on_key_press(self.data)

    def connected(self):
        print(self.address, 'connected')
        wsSrv.on_open(self)

	def handle_close(self):
		print(self.address, 'closed')

########################################################

class lcd_dot:
    def __init__(self, row, col, char = ''):
        self.row = row
        self.col = col
        self.char = char

    def getchar(self):
        return self.char
    
    def getxy(self):
        return (self.row, self.col)

    def setxy(self, row, col):
        self.row = row
        self.col = col

    def getrow(self):
        return self.row

    def getcol(self):
        return self.col


    def setchar(self, char):
        self.char = char

    def matchxy(self, row, col):
        if self.row == row and self.col == col:
            return True 
        return False

class lcd_frame:

    def __init__(self, rows, cols):
        self.lcd = RPi_I2C_driver.lcd()
        self.lcd.lcd_clear()

        self.rows = rows
        self.cols = cols
        self.dots = []
        self.buffer = []

        self.pointer = lcd_dot(0, 0, POINTER_CHAR)
        self.showpointer = False

        for r in range(rows):
            rowi = []
            for c in range(cols):
                dot = lcd_dot(r, c)
                rowi.append(' ')
                self.dots.append(dot)

            self.buffer.append(rowi)


    def _lcd_set_char(self, char, row, col):

        self.lcd.lcd_display_string_pos(char, row + 1, col)

    def setchar(self, row, col , char):

        if row >= self.rows or col >= self.cols:
            return
        
        print "set {} at ({:d} {:d})".format(char, row, col)

        self.buffer[row][col] = char

        self._lcd_set_char(char, row, col)

        # for dot in self.dots:
        #     if dot.matchxy(row, col):
        #         dot.setchar(char)

    def setnumber(self, row, col, no):
        
        self.setchar(row, col, str(no))
        
    def getchar(self, row, col):

        if row >= self.rows or col >= self.cols:
            return ''

        return self.buffer[row][col]

    def getstrings(self):

        strings = []

        for row in range(self.rows):
            rowi = ''
            for col in range(self.cols):
                rowi += self.buffer[row][col]
            strings.append(rowi)

        return strings

    def setpointer(self, row, col):

        if row >= self.rows or col >= self.cols:
            return

        self.offpointer()
        
        self.pointer.setxy(row, col)
        self.pointer.setchar(self.getchar(row, col))
        
        self.onpointer()

    def togglepointer(self):

        row = self.pointer.getrow()
        col = self.pointer.getcol()

        if self.pointer.getchar() == POINTER_CHAR:
            self.pointer.setchar(self.getchar(row, col))

        if self.showpointer:
            self.showpointer = False

            self._lcd_set_char(self.getchar(row, col), row, col)

        else:
            self.showpointer = True
            self._lcd_set_char(POINTER_CHAR, row, col)

    def offpointer(self):
        self.showpointer = True 
        self.togglepointer()

    def onpointer(self):
        self.showpointer = False 
        self.togglepointer()

    def up_ptr(self):
        row = self.pointer.getrow() - 1
        col = self.pointer.getcol()
        if row < 0:
            row = self.rows - 1
        
        self.setpointer(row, col)

    def down_ptr(self):
        row = self.pointer.getrow() + 1
        col = self.pointer.getcol()
        if row >= self.rows:
            row = 0
        
        self.setpointer(row, col)

    def right_ptr(self):
        row = self.pointer.getrow()
        col = self.pointer.getcol() + 1
        if col >= self.cols:
            col = 0
            row = row + 1
            if row >= self.rows: 
                row = 0

        self.setpointer(row, col)    

    def left_ptr(self):
        row = self.pointer.getrow()
        col = self.pointer.getcol() - 1
        if col < 0:
            col = self.cols - 1
            row = row - 1
            if row < 0: 
                row = self.rows - 1

        self.setpointer(row, col)   
             
    def data_ptr_add(self, char):
        self.setchar(self.pointer.getrow(), self.pointer.getcol(), char)
        self.right_ptr()
    
    def data_ptr_del(self):
        self.setchar(self.pointer.getrow(), self.pointer.getcol(), ' ')
        self.left_ptr()

    def on_key_press(self, val):

        if   val == "up":
            self.up_ptr()
        elif val == "down":
            self.down_ptr()
        elif val == "right":
            self.right_ptr()
        elif val == "left":
            self.left_ptr()
        elif val == "space":
            self.data_ptr_add(' ')
        elif val == "backspace":
            self.data_ptr_del()
        elif val == "enter":
            buffer = self.getstrings()
            print "SEND ", buffer
            for index in range(len(buffer)):
                wsSrv.matrix_send(str(index) + buffer[index])

        elif len(val) == 1 and val >= '!' and val <= '~':
            self.data_ptr_add(val)
   



if __name__ == "__main__":

    lcd = lcd_frame(ROWS_NO, COLS_NO)

    print "WebSocket server started, port {}".format(WSPORT)
	
    wsSrv = WsServer()
    server = WebSocketServer('', WSPORT, WsClient)
    def server_run(*args):
        server.serve_forever()

    thread.start_new_thread(server_run, ())

    while True:

        time.sleep(2)

        # wsSrv.matrix_send("00hello")
        # wsSrv.matrix_send("11hello")

