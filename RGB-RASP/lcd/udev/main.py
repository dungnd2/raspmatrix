import os
import pyudev
context = pyudev.Context()
for device in context.list_devices(): 
    print device

monitor = pyudev.Monitor.from_netlink(context)
monitor.filter_by(subsystem='usb')

for device in iter(monitor.poll, None):
    if device.action == 'add':
        print('{} connected'.format(device))
        os.system("sudo systemctl restart matrix.keyboard")