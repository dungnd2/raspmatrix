
import sys
# sys.path.append('./keyboard')
import keyboard

import signal
import time
import os

import json
try:
    import thread
except ImportError:
    import _thread as thread


import wsclient

WSPORT = 8770
WSSERVER = "ws://localhost:8770"

class KeyBoard:

    def __init__(self):
        self.ws = wsclient.WsCli(WSSERVER)

    def start(self):

        self.ws.start()
        keyboard.on_press(self.on_key_press)

    def on_key_press(self, event):

        ev = json.loads(event.to_json())
        val = ev['name']

        self.ws.send(val)
        print val


if __name__ == "__main__":

    keyinput = KeyBoard()
    keyinput.start()

    while True:
        time.sleep(1)

