import websocket
try:
    import thread
except ImportError:
    import _thread as thread
import time

SERVER = "ws://localhost:8770"

class WsCli:
    def __init__(self, server, received = None, closed = None):
        websocket.enableTrace(True)
        self.server = server

        self.received = received

        self.closed = closed

        self.ws = websocket.WebSocketApp(self.server,
                              on_open = self.on_open,
                              on_message = self.on_message,
                              on_error = self.on_error,
                              on_close = self.on_close)

        
    def start(self):

        def run(*args):
            self.ws.run_forever()

        thread.start_new_thread(run, ())

    def send(self, messaage):
        self.ws.send(messaage)

    def on_open(self):
        print ("connected to ", self.server)


    def on_message(self, message):
        print ("on_message:", message)
        if self.received != None:
            self.received(message)

    
    def on_error(self, error):
        print ("error:", error)
    
    def on_close(self):
        print ("disconnected to ", self.server)
        self.__init__(self.server, self.received)
        if self.closed != None:
            self.closed()

        
if __name__ == "__main__":

    def received(message):
        print ("received ", message)

    def on_close():
        wsCli = WsCli(SERVER, received)
    wsCli = WsCli(SERVER, received)
    wsCli.start()

    while(1):
        time.sleep(1)
        wsCli.send("hello ")
        