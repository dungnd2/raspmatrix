
import sys
import keyboard

import signal
import time
import os
# import keyboard
import json
try:
    import thread
except ImportError:
    import _thread as thread

import threading 

from simple_websocket_server import WebSocketServer, WebSocket

##########################################################
wsclients = []

def ws_send(mess):
    if len(wsclients) > 0:
        print ("send: ", mess)
        wsclients[0].send_message(mess)

class WsClient(WebSocket):

    def handle(self):
    # self.send_message(self.data)
        print "received {}".format(self.data)

    def connected(self):
        print(self.address, 'connected')
        if len(wsclients) == 0:
            wsclients.append(self)
        else:
            wsclients[0] = self

	def handle_close(self):
		print(self.address, 'closed')

def server_thread():
	print "WebSocket server started, port {}".format(8770)
	server = WebSocketServer('', 8770, WsClient)
	server.serve_forever()

########################################################

if __name__ == "__main__":

    thread.start_new_thread(server_thread, ())
   
    while True:

        time.sleep(1)

