

class Handler:

    def handle(self, message):
        print "handle ", message

class Observer:
    def __init__(self, handler = None):
        self.handler = handler

    def received(self, message):

        print "received ", message

        if self.handler != None:
            self.handler(message)


handler = Handler()
observer = Observer(handler.handle)

observer.received("hello")
